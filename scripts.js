jQuery(document).ready(function ($) {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gDRINK_LIST_URL = "http://42.115.221.44:8080/devcamp-pizza365/drinks";

  let gDrinkOptions = [];

  let gDataMenuPizza = {
    combo: [
      {
        size: "S",
        kichCo: "20",
        suon: 2,
        salad: "200g",
        nuocNgot: 2,
        thanhTien: "150000",
      },
      {
        size: "M",
        kichCo: "25",
        suon: 4,
        salad: "300g",
        nuocNgot: 3,
        thanhTien: "200000",
      },
      {
        size: "L",
        kichCo: "30",
        suon: 8,
        salad: "500g",
        nuocNgot: 4,
        thanhTien: "250000",
      },
    ],
    types: ["seafood", "hawaii", "bacon"],
    chooseCombo: function () {
      onClickGetComboPizza();
    },
    choosePizzaType: function () {
      onClickGetPizzaType();
    },
    loadDrinkOption: function () {
      $.ajax({
        type: "GET",
        url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
        success: function (response) {
          console.log(response);
          gDrinkOptions = response;
          addOptionToSelect(gDrinkOptions);
          // them su kien nhan biet thay doi cua drink field
        },
        error: function (err) {
          console.log(err);
        },
      });
    },
    getDrinkOption: function () {
      // function bat su kien on change cua gOrderObj
      $("#select-drink").on("change", (e) => {
        gDataOrderPizza.loaiNuocUong = e.target.value;
        console.log(gDataOrderPizza);
      });
    },
  };

  let gDataOrderPizza = {};

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading();

  // goi ham xu ly khi an nut send order
  $("#form-data-submit").on("submit", (e) => {
    e.preventDefault();
    onBtnSendOrderClick();
  });

  // goi ham xy ly kh ian nut create order
  $("#form-data-modal-submit").on("submit", (e) => {
    e.preventDefault();
    onBtnCreateOrderClick();
  });
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    // lay ddata combo pizza
    gDataMenuPizza.chooseCombo();
    // lay ddata pizza type
    gDataMenuPizza.choosePizzaType();
    // load do uong vao select
    gDataMenuPizza.loadDrinkOption();
    gDataMenuPizza.getDrinkOption();
  }

  //khai báo hàm thực thi khi ấn nút chuyển màu MENU COMBO(size pizza) + lay du lieu
  function onClickGetComboPizza() {
    // lựa chọn tất cả phần tử có class là btn btn-success
    let vBtnArrayElements = $(".pizza__btn__choose.btn.btn-block");
    // Duyệt mảng các phần tử nút được chọn sau đó thêm sự kiện click vào mỗi phần tử
    vBtnArrayElements.on("click", (paramElement) => {
      // chuyển màu cho nút
      paramElement.target.className =
        "btn btn-success btn-pizza-choose btn-block";
      // sau khi đổi màu nút, ta sẽ có vài nút có class là btn-warning, để duyệt mảng các nút đó ta sử dụng []
      [].forEach.call(
        $(".btn.btn-success.btn-pizza-choose.btn-block"),
        (cI) => {
          if (cI == paramElement.target) {
            // lấy dữ liệu của combo khi ấn chọn
            getDataComboPizza(cI);
          }
          if (cI !== paramElement.target) {
            // khi ấn nút khác thì đổi màu phần tử button về như cũ
            cI.className = "pizza__btn__choose btn-pizza-choose btn btn-block";
          }
        }
      );
    });
  }

  //khai báo hàm thực thi khi ấn nút chuyển màu MENU COMBO(size pizza) + lay du lieu
  function onClickGetPizzaType() {
    // lựa chọn tất cả phần tử có class là btn btn-success

    let vBtnArrayPizzaTypeBtns = $(
      ".pizza__btn__choose__pizza.btn-pizza-type.btn.btn-block"
    );
    // Duyệt mảng các phần tử nút được chọn sau đó thêm sự kiện click vào mỗi phần tử
    vBtnArrayPizzaTypeBtns.on("click", (paramElement) => {
      // chuyển màu cho nút
      paramElement.target.className =
        "btn btn-success btn-pizza-type  btn-block";
      // sau khi đổi màu nút, ta sẽ có vài nút có class là btn-warning, để duyệt mảng các nút đó ta sử dụng []
      [].forEach.call($(".btn.btn-success.btn-pizza-type.btn-block"), (cI) => {
        if (cI == paramElement.target) {
          // lấy dữ liệu của combo khi ấn chọn
          getDataPizzaType(cI);
        }
        if (cI !== paramElement.target) {
          // khi ấn nút khác thì đổi màu phần tử button về như cũ
          cI.className =
            "pizza__btn__choose__pizza btn-pizza-type btn btn-block";
        }
      });
    });
  }

  // khai bao function xu ly an nut send order
  function onBtnSendOrderClick() {
    "use strict";
    //B1: get data

    gDataOrderPizza.hoTen = $("#inp-name").val().trim();
    gDataOrderPizza.email = $("#inp-email").val().trim();
    gDataOrderPizza.soDienThoai = $("#inp-phone").val().trim();
    gDataOrderPizza.diaChi = $("#inp-address").val().trim();
    gDataOrderPizza.loiNhan = $("#inp-msg").val().trim();
    gDataOrderPizza.idVoucher = $("#inp-voucher").val().trim();

    console.log(gDataOrderPizza);
    // b2: validate
    let vIsValid = isValidFormData(gDataOrderPizza);
    if (vIsValid) {
      // b3: handle data
      checkVoucherCode(gDataOrderPizza);
      // b4 : show frontend
      $("#div-modal").modal("show");
    }
  }

  //khai bao function xy ny nut create order
  function onBtnCreateOrderClick() {
    "use strict";
    //B1: read data
    console.log(gDataOrderPizza);
    //B2: validate
    //B3: sendrequest
    sendRequestCreateOrder(gDataOrderPizza);
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */

  // khai bao function get Combo Pizza
  function getDataComboPizza(paramElement) {
    "use strict";
    gDataMenuPizza.combo.map((bI) => {
      switch (bI.size) {
        case paramElement.dataset.size:
          gDataOrderPizza.size = bI.size;
          gDataOrderPizza.nuocNgot = bI.nuocNgot;
          gDataOrderPizza.salad = bI.salad;
          gDataOrderPizza.suon = bI.suon;
          gDataOrderPizza.thanhTien = bI.thanhTien;
          gDataOrderPizza.kichCo = bI.kichCo;
          break;
      }
    });
    console.log(gDataOrderPizza);
  }

  // khai bao function get Pizza Type
  function getDataPizzaType(paramElement) {
    "use strict";
    gDataMenuPizza.types.map((bI) => {
      switch (bI) {
        case paramElement.dataset.type:
          gDataOrderPizza.loaiPizza = bI;
          console.log(gDataOrderPizza);
          break;
      }
    });
  }

  // add option to select
  function addOptionToSelect(paramRespone) {
    "use strict";
    paramRespone.map((bI) => {
      $("#select-drink").append(
        $("<option />", {
          text: `${bI.tenNuocUong}`,
          value: `${bI.maNuocUong}`,
        })
      );
    });
  }

  function isValidFormData(paramForm) {
    // Check nếu khách hàng chưa chọn cỡ pizza
    if (paramForm.size == null) {
      alert("Chưa chọn combo pizza");
      return false;
    }
    // Check nếu khách hàng chưa chọn loại pizza
    if (paramForm.loaiPizza == null) {
      alert("Chưa chọn loại pizza");
      return false;
    }
    // Check nếu khách hàng chưa chọn do uong
    if (paramForm.loaiNuocUong == null) {
      alert("Chưa chọn nước uống");
      return false;
    }

    // Check nếu khách hàng chưa nhập tên
    if (paramForm.hoTen == "") {
      alert("Chưa nhập họ tên");
      return false;
    }

    // Check nếu khách hàng chưa nhập email
    if (paramForm.email == "") {
      alert("Chưa nhập Email");
      return false;
    }

    // Check nếu khách hàng chưa nhập phone
    if (isNaN(paramForm.soDienThoai)) {
      alert("Chưa nhập số điện thoại");
      return false;
    }

    // Check nếu khách hàng chưa nhập địa chỉ
    if (paramForm.diaChi == "") {
      alert("Chưa nhập địa chỉ");
      return false;
    }

    // Check nếu khách hàng chưa nhập message
    if (paramForm.loiNhan == "") {
      console.log("Chưa nhập lời nhắc");
    }

    // Check nếu khách hàng chưa nhập voucher
    if (paramForm.idVoucher == "") {
      console.log("Chưa nhập lời nhắc");
    }
    return true;
  }

  // khai bao function check voucher
  function checkVoucherCode(paramForm) {
    "use strict";
    const vBASE_URL =
      "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/";

    $.ajax({
      type: "GET",
      url: vBASE_URL + paramForm.idVoucher,
      dataType: "json",
      success: function (response) {
        console.log(response);
        paramForm.idVoucher = response.maVoucher;
        paramForm.phanTramGiamGia = response.phanTramGiamGia;
        loadDataOrderToModal(paramForm);
      },
      error: function (err) {
        alert("Mã giảm giá không tồn tại");
        console.log(err);
        gDataOrderPizza.idVoucher = "";
        paramForm.phanTramGiamGia = "0";
        loadDataOrderToModal(paramForm);
      },
    });
  }

  //khai bao function load data len modal

  function loadDataOrderToModal(paramForm) {
    $("#inp-modal-name").val(paramForm.hoTen);
    $("#inp-modal-email").val(paramForm.email);
    $("#inp-modal-phone").val(paramForm.soDienThoai);
    $("#inp-modal-address").val(paramForm.diaChi);
    $("#inp-modal-msg").val(paramForm.loiNhan);
    paramForm.idVoucher == ""
      ? $("#inp-modal-voucher").val("")
      : $("#inp-modal-voucher").val(paramForm.idVoucher);
    $(
      "#inp-modal-info"
    ).text(`Xác nhận: ${paramForm.hoTen}, Số điện thoại: ${paramForm.soDienThoai}, Địa chỉ: ${paramForm.diaChi}, Email: ${paramForm.email}
      Menu: Size: ${
        paramForm.size
      }, Sườn nướng: ${paramForm.suon}, Số lượng nước: ${paramForm.nuocNgot}
      Loại pizza: ${
        paramForm.loaiPizza
      }, Thành tiền: ${paramForm.thanhTien} VND, Mã giảm giá: ${paramForm.idVoucher == "" ? "" : paramForm.idVoucher}
      Phải thanh toán: ${
        isNaN(paramForm.phanTramGiamGia)
          ? (paramForm.thanhTien = paramForm.thanhTien)
          : (paramForm.thanhTien =
              paramForm.thanhTien *
              ((100 - parseInt(paramForm.phanTramGiamGia)) / 100))
      } VND
      `);
  }

  //khai bao function sendrequest create order
  function sendRequestCreateOrder(paramForm) {
    "use strict";
    const vBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";

    var vObjectRequest = {
      kichCo: paramForm.size,
      duongKinh: paramForm.kichCo,
      suon: paramForm.suon,
      salad: paramForm.salad,
      loaiPizza: paramForm.loaiPizza,
      idVourcher: paramForm.idVoucher,
      idLoaiNuocUong: paramForm.loaiNuocUong,
      soLuongNuoc: paramForm.soLuongNuoc,
      hoTen: paramForm.hoTen,
      thanhTien: paramForm.thanhTien,
      email: paramForm.email,
      soDienThoai: paramForm.soDienThoai,
      diaChi: paramForm.diaChi,
      loiNhan: paramForm.loiNhan,
    };

    $.ajax({
      type: "POST",
      url: vBASE_URL,
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(vObjectRequest),
      dataType: "json",
      success: function (response) {
        console.log(response);
        $("#div-modal").modal("hide");
        $("#div-modal-success").modal("show");
        $("#inp-order-code").val(response.orderId);
      },
      error: function (err) {
        console.log(err);
      },
    });
  }
});
